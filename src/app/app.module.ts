import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReleasesComponent } from './components/releases/releases.component';
import { ArtistsComponent } from './components/artists/artists.component';
import { LabelsComponent } from './components/labels/labels.component';
import { CountriesComponent } from './components/countries/countries.component';
import { MarkersComponent } from './components/markers/markers.component';
import { MapComponent } from './components/map/map.component';
import { HomeComponent } from './components/home/home.component';
import { HttpClientModule } from '@angular/common/http';

import {ReleasesService} from './services/releases.service';
import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import { RegisterComponent } from './components/register/register.component';
import {FormsModule} from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    ReleasesComponent,
    ArtistsComponent,
    LabelsComponent,
    CountriesComponent,
    MapComponent,
    HomeComponent,
    MarkersComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LeafletMarkerClusterModule,
    LeafletModule,
    FormsModule,
  ],
  providers: [
    ReleasesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
