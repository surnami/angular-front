import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Release} from '../models/Release';
import {Position} from '../models/Position';

@Injectable({
  providedIn: 'root'
})

export class MapService {
  API_URI = 'http://localhost:3030/locations';
  constructor(private httpClient: HttpClient) { }
  getPositions(page: number, size: number): Observable<Position> {
    return this.httpClient.get(this.API_URI + '?page=' +  page + '&size=' + size);
  }
  editPosition(id: string, updatedPosition: Position): Observable<Position> {
    return this.httpClient.put(`${this.API_URI}/${id}`, updatedPosition);
  }
}
