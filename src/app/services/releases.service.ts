import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Release } from '../models/Release';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReleasesService {
  API_URI = 'http://localhost:3030/albums';
  constructor(private httpClient: HttpClient) { }
  getReleases(page: number, size: number): Observable<any>{
    return this.httpClient.get(this.API_URI + '?page=' + page + '&size=' + size);
  }
  getRelease(id: string): Observable<any> {
    return this.httpClient.get(`${this.API_URI}/${id}`);
  }
  deleteRelease(id: string): Observable<any> {
    return this.httpClient.delete(`${this.API_URI}/${id}`);
  }
  addRelease(release: Release): Observable<any> {
    return this.httpClient.post(this.API_URI, release);
  }
  editRelease(id: string, updatedRelease: Release): Observable<any> {
    return this.httpClient.put(`${this.API_URI}/${id}`, updatedRelease);
  }
}
