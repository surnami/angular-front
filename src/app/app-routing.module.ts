import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReleasesComponent } from './components/releases/releases.component';
import { LabelsComponent } from './components/labels/labels.component';
import { ArtistsComponent } from './components/artists/artists.component';
import { CountriesComponent } from './components/countries/countries.component';
import { MapComponent } from './components/map/map.component';
import { HomeComponent } from './components/home/home.component';
import { MarkersComponent } from './components/markers/markers.component';
import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';
import {ProfileComponent} from './components/profile/profile.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'map',
    component: MapComponent
  },
  {
    path: 'releases',
    component: ReleasesComponent
  },
  {
    path: 'countries',
    component: CountriesComponent
  },
  {
    path: 'labels',
    component: LabelsComponent
  },
  {
    path: 'artists',
    component: ArtistsComponent
  },
  {
    path: 'markers', component: MarkersComponent
  },
  {
    path: 'register', component: RegisterComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'profile', component: ProfileComponent
  },
  {
    path: '', redirectTo: '/map', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
