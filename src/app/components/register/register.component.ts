import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../../services/register.service';

@Component({
  selector: 'app-login',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {
    username: null,
    email: null,
    password: null
  };
  isSuccessful = false;
  isFaulty = false;
  errorMessage = '';
  constructor(private registerService: RegisterService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    const { username, email, password } = this.form;

    this.registerService.register(username, email, password).subscribe(
      data => {
        this.isSuccessful = true;
        this.isFaulty = false;
        this.redirectToHomepage();
      },
      err => {
        this.errorMessage = err.error.content;
        this.isSuccessful = false;
        this.isFaulty = true;
      }
    );
  }
  redirectToHomepage(): void {
    setTimeout(() => {
      window.location.href = 'http://localhost:4200/';
    }, 8888);
  }
}
