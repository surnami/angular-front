import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import {MapService} from '../../services/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {
  map: any = L.Map;
  markers: any;
  positions: any = [];
  pages: any = [];
  currentPage = 0;
  totalPages = 0;
  size = 0;
  constructor(private mapService: MapService) {
  }

  ngOnInit(): void {
    this.positions = [];
    L.Icon.Default.imagePath = 'assets/leaflet/';
    this.currentPage = 1;
    this.size = 20;
    this.map = L.map('map').setView([50, 50], 3);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'surなみ',
    }).addTo(this.map);
    this.markers = L.markerClusterGroup({removeOutsideVisibleBounds: true});
    this.onGetLocations();
  }

  onGetLocations(): void {
    this.mapService.getPositions(this.currentPage, this.size).subscribe(
      data => {
        this.positions = data.locations;
        this.totalPages = data.totalPages;
        this.pages = new Array<number>(this.totalPages);
        this.onPlaceMarkers();
      },
      (err: any) => {
        console.error(err);
      }
    );
  }

  onPlaceMarkers(): void {
    for (const p in this.positions) {
      if (p != null) {
        this.markers.addLayer(L.marker([this.positions[p].latitude, this.positions[p].longitude]));
      }
    }
    this.map.addLayer(this.markers);
  }

  onPageLocation(i: number): void {
    this.markers.clearLayers();
    this.currentPage = i;
    this.onGetLocations();
  }
}
