import { Component, OnInit } from '@angular/core';
import {ReleasesService} from '../../services/releases.service';

@Component({
  selector: 'app-releases',
  templateUrl: './releases.component.html',
  styleUrls: ['./releases.component.css']
})
export class ReleasesComponent implements OnInit {
  releases: any = [];
  pages: any = [];
  currentPage = 0;
  size = 0;
  totalPages = 0;
  constructor(private releasesService: ReleasesService) { }
  ngOnInit(): void {
    this.releases = [];
    this.currentPage = 1;
    this.size = 20;
    this.onGetReleases();
  }
  onGetReleases(): void {
    this.releases = [];
    this.releasesService.getReleases(this.currentPage, this.size).subscribe(
      data => {
        this.releases = data.albums;
        if (data.totalPages > 25) {
          this.totalPages = 25;
        } else {
          this.totalPages = data.totalPages;
        }
        this.pages = new Array<number>(this.totalPages);
      },
      err => {
        console.error(err);
      }
    );
  }

  onPageRelease(i: number): void {
    this.currentPage = i;
    this.onGetReleases();
  }
}
