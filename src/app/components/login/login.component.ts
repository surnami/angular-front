import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../services/login.service';
import {TokenService} from '../../services/token.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };
  isLogged = false;
  isAnonymous = false;
  errorMessage = '';
  roles: string[] = [];
  constructor(private loginService: LoginService, private tokenService: TokenService) { }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
      this.roles = this.tokenService.getUser().roles;
    }
  }
  onSubmit(): void {
    const { username, password } = this.form;
    this.loginService.login(username, password).subscribe(
      data => {
        console.log(data);
        this.tokenService.saveToken(data.accessToken);
        this.tokenService.saveUser(data);

        this.isAnonymous = false;
        this.isLogged = true;
        this.roles = this.tokenService.getUser().roles;
        this.redirectToProfile();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isAnonymous = true;
      }
    );
  }
  redirectToProfile(): void {
    setTimeout(() => {
      window.location.href = 'http://localhost:4200/profile';
    }, 2222);
  }
}
