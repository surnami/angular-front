export interface Release {
  totalPages: any;
  albums?: any;
  id?: string;
  name?: string;
  artist?: any;
  label?: any;
  sleeveDesignUrl?: string;
  networks?: string[];
  date?: Date;
}
