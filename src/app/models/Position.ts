export interface Position {
  totalPages?: any;
  locations?: any;
  latitude?: string;
  longitude?: string;
  name?: string;
}
