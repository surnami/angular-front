FROM nginx:alpine

WORKDIR /app
LABEL name="nginx container"
LABEL version="1.0"
LABEL architecture="x86_64"

COPY /dist/app /usr/share/nginx/html

EXPOSE 80

